// validation

// function show message
function showMessage(idErr, message) {
  document.getElementById(idErr).style.display = 'block';
  domID(idErr).innerHTML = message;
}

// check duplicate
function isDuplicate(listEmployee, inforEmployee, keyName, idErr, tagErr) {
  var foundIndex = listEmployee.findIndex(
    (item) => item[keyName] === inforEmployee[keyName]
  );
  console.log('inforEmployee: ', inforEmployee);
  console.log('key name: ', inforEmployee[keyName]);

  if (foundIndex !== -1) {
    showMessage(idErr, tagErr + ` đã tồn tại`);
    return false; // duplicated
  }
  showMessage(idErr, ``);
  return true; // okey
}

// check length
function isLength(inforEmployee, keyName, idErr, tagErr, min, max) {
  if (
    inforEmployee[keyName].length >= min &&
    inforEmployee[keyName].length <= max
  ) {
    showMessage(idErr, ``);
    return true;
  } else if (inforEmployee[keyName].length === 0) {
    showMessage(idErr, tagErr + ` không được để trống`);
  } else {
    showMessage(idErr, tagErr + ` phải có độ dài từ ${min} đến ${max}`);
    return false;
  }
}

// 1. validate user name

// 2. validate full name
function isFullName(fullName) {
  const regFullName =
    /^[aAăắằẳẵặâấầẩẫậeêếềểễệiíìỉĩịoôốồổỗộơớờởỡợuúùủũụyýỳỷỹỵA-Za-z\s]{2,}$/;

  if (regFullName.test(fullName)) {
    showMessage('tbTen', '');
    return true;
  } else if (fullName.length === 0) {
    showMessage('tbTen', 'Trường này không được để trống');
    return false;
  } else {
    showMessage(
      'tbTen',
      'Họ và tên không hợp lệ, tên nhân viên phải có ít nhất 2 ký tự và chỉ chứa chữ cái tiếng Việt'
    );
    return false;
  }
}

// 3. validate email
function isEmail(email) {
  const regEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
  if (regEmail.test(email)) {
    showMessage('tbEmail', '');
    return true;
  } else if (email.length === 0) {
    showMessage('tbEmail', 'Email không được để trống');
    return false;
  } else {
    showMessage('tbEmail', 'Email không hợp lệ');
    return false;
  }
}

// validate password
function isPassword(pass) {
  const regPass = /^(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,10}$)/;
  if (regPass.test(pass)) {
    showMessage('tbMatKhau', '');
    return true;
  } else if (pass.length === 0) {
    showMessage('tbMatKhau', 'Password không được để trống');
    return false;
  } else {
    showMessage(
      'tbMatKhau',
      'Password phải chứa từ 6 -10 ký tự, và chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt'
    );
    return false;
  }
}

// validate work date

function isWorkDay(workDay) {
  // Regex for validating date format according to mm/dd/yyyy
  const regWorkDay = /^\d{2}\/\d{2}\/\d{4}$/;

  if (regWorkDay.test(workDay)) {
    showMessage('tbNgay', '');
    return true;
  } else if (workDay.length == 0) {
    showMessage('tbNgay', 'Ngày làm không được để trống');
    return false;
  } else {
    showMessage('tbNgay', 'Ngày làm phải theo định dạng mm/dd/yyy');
    return false;
  }
}

// validate basicSalary
function isBasicSalary(basicSalary) {
  if (basicSalary >= 1000000 && basicSalary <= 20000000) {
    showMessage('tbLuongCB', '');
    return true;
  } else if (basicSalary.length === 0) {
    showMessage('tbLuongCB', 'Lương cơ bản không được để trống');
    return false;
  } else {
    showMessage('tbLuongCB', 'Lương cơ bản phải từ 1.000.000 đến 20.000.000');
    return false;
  }
}

// validate position
function isPosition(position) {
  if (
    position === 'Sếp' ||
    position === `Trưởng phòng` ||
    position === `Nhân viên`
  ) {
    showMessage('tbChucVu', '');
    return true;
  }
  showMessage('tbChucVu', 'Chưa chọn chức vụ');
  return false;
}
// validate worktime
function isWorkTime(workTime) {
  if (workTime >= 80 && workTime <= 200) {
    showMessage('tbGiolam', '');
    return true;
  } else if (workTime.length === 0) {
    showMessage('tbGiolam', 'Giờ làm không được để trống');
    return false;
  } else {
    showMessage('tbGiolam', 'Giờ làm phải từ 80 đến 200');
    return false;
  }
}

// validare function update
function isUserNameUpdate(listEmployee, inforEmployee, keyName, idErr, tagErr) {
  var foundIndex = listEmployee.findIndex(
    (item) => item[keyName] === inforEmployee[keyName]
  );
  if (foundIndex !== -1) {
    showMessage(idErr, ` `);
    return true; // duplicated
  }
  showMessage(idErr, `không được thay đổi User Name`);
  return false; // okey
}
function isEmailUpdate(listEmployee, inforEmployee, keyName, idErr, tagErr) {
  var foundIndex = listEmployee.findIndex(
    (item) => item[keyName] === inforEmployee[keyName]
  );
  if (foundIndex !== -1) {
    showMessage(idErr, ` `);
    return true; // duplicated
  }
  showMessage(idErr, `không được thay đổi Email`);
  return false; // okey
}
