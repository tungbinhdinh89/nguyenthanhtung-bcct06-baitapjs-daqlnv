// 1. function dom to element
function domID(id) {
  return document.getElementById(id);
}

// 2. function get value from form
function getInforFromForm() {
  var userName = domID('tknv').value;
  var fullName = domID('name').value;
  var email = domID('email').value;
  var password = domID('password').value;
  var workDay = domID('datepicker').value;
  var basicSalary = domID('luongCB').value;
  var position = domID('chucvu').value;
  var workTime = domID('gioLam').value;

  return new employee(
    userName,
    fullName,
    email,
    password,
    workDay,
    basicSalary,
    position,
    workTime
  );
}

// 3. function render listEmployee to scree
function renderListEmployee(listEmployee) {
  var contentHTML = '';
  // var i = listEmployee.length -1; i >= 0 ; i++
  for (var i = 0; i < listEmployee.length; i++) {
    var item = listEmployee[i];
    var contentTr = `
    <tr>
    <td>${item.userName}</td>
    <td>${item.fullName}</td>
    <td>${item.email}</td>
    <td>${item.workDay}</td>
    <td>${item.position}</td>
    <td>${new Intl.NumberFormat('de-DE', {
      style: 'currency',
      currency: 'VND',
    }).format(item.totalSalary())}</td>
    <td id='classify'>${item.classify()}</td>
    <td><button class='btn btn-danger' onclick='deleteEmployee("${
      item.userName
    }")'> Xoá</button></td>
    <td><button class='btn btn-warning' data-toggle="modal"
                    data-target="#myModal" onclick='editEmployee("${
                      item.userName
                    }")'> Sửa</button></td>
    </tr>
    `;
    contentHTML += contentTr;
  }
  domID('tableDanhSach').innerHTML = contentHTML;
}

// 4. function set data to local Storage e

const listEmployee_Local = 'listEmployee_Local';
function setDataToLocalStorage() {
  var dataJSON = JSON.stringify(listEmployee);
  localStorage.setItem(listEmployee_Local, dataJSON);
}

// 5. function get data from local Storage

function getDataFromLocalStorage() {
  var JSONData = localStorage.getItem(listEmployee_Local);
  if (JSONData !== null) {
    listEmployee = JSON.parse(JSONData).map(function (item) {
      return new employee(
        item.userName,
        item.fullName,
        item.email,
        item.password,
        item.workDay,
        item.basicSalary,
        item.position,
        item.workTime
      );
    });
  }
  renderListEmployee(listEmployee);
}
// function fill information employee to form
function fillInforEmployee(inforEmployee) {
  domID('tknv').value = inforEmployee.userName;
  domID('name').value = inforEmployee.fullName;
  domID('email').value = inforEmployee.email;
  domID('password').value = inforEmployee.password;
  domID('datepicker').value = inforEmployee.workDay;
  domID('luongCB').value = inforEmployee.basicSalary;
  domID('chucvu').value = inforEmployee.position;
  domID('gioLam').value = inforEmployee.workTime;
}

// function reset form
function resetForm() {
  domID('formQLNV').reset();
}
