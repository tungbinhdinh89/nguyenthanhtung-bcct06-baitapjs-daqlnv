// add more employee
var listEmployee = [];
// get data from local Storage
getDataFromLocalStorage();
function addEmployee() {
  var inforEmployee = getInforFromForm();
  var isValid =
    // check user name
    isDuplicate(
      listEmployee,
      inforEmployee,
      'userName',
      'tbTKNV',
      'Username'
    ) && isLength(inforEmployee, 'userName', 'tbTKNV', 'Username', 4, 6);
  isValid &= isFullName(inforEmployee.fullName);

  console.log('sun log: ', isFullName(inforEmployee.fullName));

  // check email
  isValid &=
    isDuplicate(listEmployee, inforEmployee, 'email', 'tbEmail', 'Email') &&
    isEmail(inforEmployee.email);
  // check password
  isValid &= isPassword(inforEmployee.password);
  // check work day
  isValid &= isWorkDay(inforEmployee.workDay);
  // check basic salary
  isValid &= isBasicSalary(inforEmployee.basicSalary);
  // check position
  isValid &= isPosition(inforEmployee.position);
  // check worktime
  isValid &= isWorkTime(inforEmployee.workTime);

  if (isValid) {
    listEmployee.push(inforEmployee);
    setDataToLocalStorage();
    renderListEmployee(listEmployee);
    resetForm();
  }
}

// delete employee
function deleteEmployee(userName) {
  var foundIndex = listEmployee.findIndex(function (item) {
    return item.userName === userName;
  });
  if (foundIndex !== -1) {
    listEmployee.splice(foundIndex, 1);
    setDataToLocalStorage();
    renderListEmployee(listEmployee);
  }
}

function editEmployee(userName) {
  var foundIndex = listEmployee.findIndex((item) => item.userName === userName);

  inforEmployee = listEmployee[foundIndex];

  fillInforEmployee(inforEmployee);
}

// update employee
function updateEmployee() {
  var inforEmployee = getInforFromForm();
  console.log('inforEmployee after: ', inforEmployee);
  console.log('inforEmployee.userName: ', inforEmployee.userName);

  var foundIndex = listEmployee.findIndex(
    (item) => item.userName === inforEmployee.userName
  );
  var isValid = isUserNameUpdate(
    listEmployee,
    inforEmployee,
    'userName',
    'tbTKNV',
    'Username'
  );
  isValid &=
    isEmail(inforEmployee.email) &&
    isDuplicate(listEmployee, inforEmployee, 'email', 'tbEmail', 'Email');
  // check password
  isValid &= isPassword(inforEmployee.password);
  // check work day
  isValid &= isWorkDay(inforEmployee.workDay);
  // check basic salary
  isValid &= isBasicSalary(inforEmployee.basicSalary);
  // check position
  isValid &= isPosition(inforEmployee.position);
  // check worktime
  isValid &= isWorkTime(inforEmployee.workTime);

  if (isValid) {
    listEmployee[foundIndex] = getInforFromForm();
    console.log('listEmployee: ', listEmployee);
    console.log('inforEmployee: ', inforEmployee);
    renderListEmployee(listEmployee);
  }
}

// classify employee

function classify() {
  console.log('yes');
  var getSearch = domID('searchName').value;
  var classify = listEmployee.filter((item) => item.classify() === getSearch);
  console.log('classify: ', classify);
  if (classify.length > 0) {
    listEmployee = classify;
    renderListEmployee(listEmployee);
  }
}
